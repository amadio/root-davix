ROOT Davix Package
==================

This package contains the equivalent of ROOT's RDAVIX library, but built
externally. It is only meant to be an example of how to build a library with a
generated ROOT dictionary with modern CMake constructs, and therefore does not
have any install targets.

The new macro added here, called `add_dictionary()` can be used to add a ROOT
generated dictionary to an existing library target, by generating the source
file and appending it to the list of sources of the library itself. The macro is
such that include directories are inherited from the library target itself,
depedencies are automatic, and the order in which things are called does not
matter. This is because the list of include directories is not generated when
CMake generates the build system, but at a later time, from the properties of
the targets as seen by the generated build system when configuring and building.

