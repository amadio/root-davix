function(add_dictionary)
  cmake_parse_arguments(G "" "TARGET;LINKDEF" "HEADERS;OPTIONS" "${ARGN}")
  set(G_DEFINES "$<TARGET_PROPERTY:${G_TARGET},COMPILE_DEFINITIONS>")
  set(G_INCLUDES "$<TARGET_PROPERTY:${G_TARGET},INCLUDE_DIRECTORIES>")

  target_include_directories(${G_TARGET} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
  target_include_directories(${G_TARGET} PRIVATE ${CMAKE_CURRENT_BINARY_DIR})

  add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/G_${G_TARGET}.cxx 
    BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/G_${G_TARGET}.cxx DEPENDS ${G_HEADERS}
    COMMAND ${ROOT_rootcling_CMD} -f
      ${CMAKE_CURRENT_BINARY_DIR}/G_${G_TARGET}.cxx ${G_OPTIONS}
      "$<$<BOOL:${G_DEFINES}>:-D$<JOIN:${G_DEFINES},;-D>>"
      "$<$<BOOL:${G_INCLUDES}>:-I$<JOIN:${G_INCLUDES},;-I>>"
      ${G_HEADERS} ${G_LINKDEF} COMMAND_EXPAND_LISTS)
  target_sources(${G_TARGET} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/G_${G_TARGET}.cxx)
endfunction()

function(add_root_library name)
  cmake_parse_arguments(R "NODICT;NOPCM;NOMAP" "LINKDEF" "HEADERS;SOURCES" ${ARGN})

  add_library(${name} ${R_SOURCES})

  if(NOT R_LINKDEF)
    set(R_LINKDEF LinkDef.h)
  endif()

  add_dictionary(TARGET ${name} HEADERS ${R_HEADERS} LINKDEF ${R_LINKDEF})
endfunction()
